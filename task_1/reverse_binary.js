function Convert() {
    const input = document.getElementById('numberToReverse');
    const binaryNumberInput = document.getElementById('binaryNumber');
    const reverseBinaryNumberInput = document.getElementById('reverseBinaryNumber');
    const resultInput = document.getElementById('result');

    /**
     * Convert decimal number to binary number
     * @param {string} decimal number decimal to convert in binary
     */
    const toBinary = (decimal) => {
        for (let i = 0; i < decimal.length; i++) {
            if (!(decimal[i]).match(/[0-9]/)) {
                return 0;
            }
        }
        let binary = "";
        while (decimal >  0) {
            binary += decimal % 2;
            decimal = parseInt(decimal / 2);
        }
        return binary.split('').reverse().join('');
    }

    /**
     * Convert binary to decimal number
     * @param {string} binary number binary to convert in decimal
     */
    const toDecimal = (binary) => {
        for (let i = 0; i < binary.length; i++) {
            if (!['0','1'].includes(binary[i])) {
                return 0;
            }
        }
        let decimal = 0;
        binary.split('').reverse().forEach((e, i) => { e === '1' ? decimal += Math.pow(2,i) : 0});
        return decimal;
    }

    /**
     * Reverse number
     * @param {string} decimal number decimal to reverse
     */
    const reverse = function (decimal) {
        return [...decimal].reverse().join("");
    }

    input.addEventListener('input', () => {
        const value = input.value;

        const binaryNumber = toBinary(value);
        binaryNumberInput.value = binaryNumber;

        const reverseBinaryNumber = reverse(binaryNumber);
        reverseBinaryNumberInput.value = reverseBinaryNumber;

        resultInput.value = toDecimal(reverseBinaryNumber);
    })
}

Convert();
