<?php

declare(strict_type=1);

final class Path
{
    private const PARENT_DIRECTORY = '..';
    private const PATH_SEPARATOR = '/';
    private const ROOT_PATH = '/';

    /**
     * @var string
     */
    private string $path;

    function __construct(string $path)
    {
        $this->path = $path;
    }

    public function getCurrentPath(): string
    {
        return trim($this->path);
    }

    public function cd(string $path): void
    {
        $path = trim($path);
        if (false === $this->isPathValid($path)) {
            throw new \InvalidArgumentException('This path should contains only letters');
        }
        if (true === $this->startsWithRootSeparator($path)) {
            $this->path = $path;
            return;
        }

        $dirs = $this->getDirsFromPath($this->path);
        $newDirs = $this->getDirsFromPath($path);

        $newPath = [];

        foreach ($newDirs as $newDir) {
            if (self::PARENT_DIRECTORY === $newDir) {
                array_pop($dirs);
            } else {
                $newPath[] = $newDir;
            }
        }

        $this->path = implode(self::PATH_SEPARATOR, array_merge($dirs, $newPath));
    }

    private function startsWithRootSeparator(string $path): bool
    {
        return self::ROOT_PATH === $path[0];
    }

    private function getDirsFromPath(string $path): array
    {
        return explode(self::PATH_SEPARATOR, $path);
    }

    private function isPathValid(string $value): bool
    {
        if ('' === $value) {
            return false;
        }
        $find = [
            self::PARENT_DIRECTORY,
            self::PATH_SEPARATOR,
            self::ROOT_PATH
        ];

        $value = str_replace($find, '', $value);

        // if path wasn't empty
        // and was removed special letters and now it's empty
        // it's mean that is correct one
        if ('' === $value) {
            return true;
        }

        return preg_match(
            '/^[a-zA-Z]+$/',
            $value
        );
    }

}

$path = new Path('/a/b/c/d');
$path->cd('../x');
echo $path->getCurrentPath();
