#! /usr/bin/python3

import json
from collections import namedtuple
from math import radians, cos, sin, asin, sqrt
from operator import attrgetter


class HaversineCoverage:
    __location = namedtuple('location', ['id', 'lat', 'lng', 'zip_code'])
    __shop = namedtuple('shop', ['id', 'lat', 'lng', 'enabled', 'coverage'])
    __locations = []
    __shops = []
    __coverage = 0

    def __init__(self, coverage):
        self.__coverage = coverage

    def run(self):
        self.__get_locations()
        self.__get_active_shops()

    def __get_locations(self):
        with open('locations.json') as json_file:
            data = json.load(json_file)
            for location in data:
                self.__locations.append(
                    self.__location(lat=location['lat'], lng=location['lng'], id=location['id'],
                                    zip_code=location['zip_code'])
                )

    def __get_active_shops(self):
        with open('shoppers.json') as json_file:
            data = json.load(json_file)
            for shop in data:
                if not shop['enabled']:
                    continue
                coverage = self.__get_coverage(lng=shop['lng'], lat=shop['lat'])
                self.__shops.append(
                    self.__shop(lat=shop['lat'], lng=shop['lng'], id=shop['id'], enabled=shop['enabled'],
                                coverage=coverage)
                )

    def __get_coverage(self, lng, lat):
        coverage = 0
        for location in self.__locations:
            if self.__haversine(lng1=location.lng, lat1=location.lat, lng2=lng, lat2=lat) < self.__coverage:
                coverage += 1
        return coverage

    def get_sorted_by_coverage(self):
        amount = 0
        shops = sorted(self.__shops.copy(), key=attrgetter('coverage'), reverse=True)
        shops_coverage = []

        for shop in shops:
            amount += shop.coverage

        if amount > 0:
            for shop in shops:
                if shop.coverage <= 0:
                    continue
                percent = (shop.coverage / amount) * 100
                shops_coverage.append({'shopper_id': shop.id, 'coverage': percent})

        return shops_coverage

    def __haversine(self, lng1, lat1, lng2, lat2):
        """
        Calculate the great circle distance between two points
        on the earth (specified in decimal degrees)
        """
        # convert decimal degrees to radians
        lng1, lat1, lng2, lat2 = map(radians, [lng1, lat1, lng2, lat2])

        # haversine formula
        dlng = lng2 - lng1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlng / 2) ** 2
        c = 2 * asin(sqrt(a))
        r = 6371  # Radius of earth in kilometers. Use 3956 for miles
        return c * r


haversineCoverage = HaversineCoverage(10)
haversineCoverage.run()
print(haversineCoverage.get_sorted_by_coverage())
